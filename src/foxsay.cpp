/**
 * @author  John Wikman
 * @file    foxsay.cpp
 * @license MIT
 * @brief   Main file for the foxsay program.
 */

#include <cstdlib>
#include <cctype>
#include <iostream>
#include <random>
#include <sstream>

#include <cppopt.h>

/**
 * @brief Parameters to define program behavior.
 */
struct foxsay_parameters {
    bool use_stdin;
    std::uint32_t lower_bound;
    std::uint32_t upper_bound;
    std::uint32_t space_sparsity;
    std::vector<std::string> words;
};

/**
 * @brief Let the fox speak.
 */
void foxsay(const std::string& word, const struct foxsay_parameters& p)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::uint32_t> countdist(p.lower_bound, p.upper_bound);
    std::uniform_int_distribution<std::uint32_t> spacedist(0, p.space_sparsity);
    std::uint32_t count = countdist(gen);

    for (std::uint32_t i = 0; i < count; ++i) {
        if (i > 0 && spacedist(gen) == 0)
            std::cout << ' ';

        std::cout << word;
    }
    std::cout << std::endl;
}

/**
 * @brief Main function.
 */
int main(int argc, char *argv[])
{
    struct foxsay_parameters p;
    p.use_stdin = false;
    p.lower_bound = 3;
    p.upper_bound = 12;
    p.space_sparsity = 2;
    p.words = {};

    auto parser = cppopt::parser("What does the fox say?");
    parser.add(
        cppopt::flag('s', "stdin", "Read words from stdin.", &p.use_stdin),
        cppopt::param<std::uint32_t>(
            'l', "lower", {"COUNT"},
            "Minimum number of printouts of each input word."
            "\n"
            "Default: 3",
            &p.lower_bound
        ),
        cppopt::param<std::uint32_t>(
            'u', "upper", {"COUNT"},
            "Maximum number of printouts of each input word."
            "\n"
            "Default: 12",
            &p.upper_bound
        ),
        cppopt::param<std::uint32_t>(
            "space-sparsity", {"COUNT"},
            "Sparsity of spaces in the output. On average how many words will"
            " be printed until the next space."
            "\n"
            "Default: 2",
            &p.space_sparsity
        ),
        cppopt::multi_positional<std::string>(
            {"WORDS"},
            "Words to be uttered by the fox.",
            &p.words,
            cppopt::NONE
        )
    );
    parser.set_optterm_token("--");
    parser.parse(argc, argv);

    // manual sanity check of input arguments
    if (p.lower_bound > p.upper_bound) {
        std::ostringstream msg;
        msg << "Lower bound (" << p.lower_bound << ") cannot be greater than "
            << "the upper bound (" << p.upper_bound << ").";
        parser.error(msg.str());
    }

    if (p.use_stdin && p.words.size() > 0)
        parser.error("Cannot specify word arguments if using stdin.");


    std::cout << "\"What does the fox say?\"" << std::endl;
    if (p.use_stdin) {
        std::ostringstream wordbuf;
        while (!std::cin.eof()) {
            int c = std::cin.get();
            if (!std::isgraph(c)) {
                if (wordbuf.tellp() > 0) {
                    foxsay(wordbuf.str(), p);
                    wordbuf.str("");
                }
            } else {
                wordbuf << (char) c;
            }
        }
        // Get the final word if it exists
        if (wordbuf.tellp() > 0)
            foxsay(wordbuf.str(), p);
    } else {
        for (const std::string& word : p.words)
            foxsay(word, p);
    }

    return 0;
}


