.PHONY: foxsay

CXX=g++
CXXFLAGS=--std=c++11 -Isubmodules/cppopt/include -O2

SOURCEFILES=src/*.cpp submodules/cppopt/src/*.cpp

foxsay:
	$(CXX) $(CXXFLAGS) $(SOURCEFILES) -o foxsay
